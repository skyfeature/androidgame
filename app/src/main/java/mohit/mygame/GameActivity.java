package mohit.mygame;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class GameActivity extends AppCompatActivity {


    GridView gridView;

    static final String[] numbers = new String[] {
            "A", "B", "C", "D", "E",
            "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //setting the orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        gridView = (GridView) findViewById(R.id.gridView);
        Double d = (Math.sqrt(numbers.length));
        Integer noOfColumn = d.intValue();
        gridView.setNumColumns(noOfColumn);

//        float xdpi = this.getResources().getDisplayMetrics().xdpi;
//        int mKeyHeight = (int) ( xdpi/4 );
//        gridView.setColumnWidth( mKeyHeight );

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.list_item, R.id.grid_item_label, this.numbers);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.list_item, this.numbers);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
//                Toast.makeText(getApplicationContext(),
//                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}